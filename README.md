#For Backend
Clone the Repository and run below commands
1. mvn clean install -Dmaven.test.skip=true
2. mvn eclipse:eclipse
3. Import the project into eclipse and work on it.

#For Front End
Clone the Repository and do the following
1. Move to Interview-Tracker-Front
2. run npm install
3. start the server using commands ng serve or npm start
