package com.interview.tracker.dao.dto;

import java.security.Timestamp;
import java.util.Date;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "candidates",uniqueConstraints = @UniqueConstraint(columnNames = {
	"candidate_id",
	"job_id"
}))
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Data
@IdClass(CandidateJobKey.class)
public class CandidateDto {

	@Id
	@Column(name = "job_id")
	private long jobId;
	@Id
	@Column(name = "candidate_id")
	private long candidateId;
	
	@Column(name="candidate_name")
	private String candidateName;
    
	@Column(name="round_number")
	private int roundNumber;
	
	@Column(name="status")
	private String status;
	
	@Column(name="email_id")
	private String emailId;
	
	@Column(name="sheduled_date")
	private Date sheduledDate; 
	
	@Column(name="sheduled_time")
	private String time;
	
	@Column(name="feed_back")
	private String feedBack;
	
}
