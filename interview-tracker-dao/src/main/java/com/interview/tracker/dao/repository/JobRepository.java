package com.interview.tracker.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.interview.tracker.dao.dto.JobDto;

public interface JobRepository extends JpaRepository<JobDto, Long> {

	/**
	 * findAllJobsByCandidateId is used to select all the jobs which are applied by
	 * the candidate.
	 * 
	 * @param candidateId, the id of the candidate
	 */
	@Query(value = "SELECT * FROM jobs job INNER JOIN candidates as candidate ON(job.job_id=candidate.job_id) "
			+ " WHERE candidate.candidate_name=:candidateName", nativeQuery = true)
	public List<JobDto> findAllJobsByCandidateName(@Param("candidateName") String candidateName);

	/**
	 * findAllUnAppliedJobs is used to select all the jobs which are not applied by
	 * the candidate.
	 * 
	 * @param candidateId, the id of the candidate
	 */
	@Query(value = "SELECT * FROM jobs job WHERE job_id NOT IN( "
			+ " SELECT job_id from candidates WHERE candidate_name=:candidateName)", nativeQuery = true)
	public List<JobDto> findAllUnAppliedJobs(@Param("candidateName") String candidateName);

	/**
	 * 
	 * @param personInCharge
	 * @return
	 */
	@Query(value = "SELECT * FROM jobs where status='OPEN' and pic=:personIncharge", nativeQuery = true)
	public List<JobDto> findAllJobsByPersonIncharge(@Param("personIncharge") long personIncharge);

	/**
	 * 
	 * @param jobName
	 * @param offset
	 * @return
	 */
	@Query(value = "SELECT * FROM jobs WHERE job_name like %:jobName% and status='OPEN' and pic=:pic limit 20 offset :offset", nativeQuery = true)
	public List<JobDto> findAllJobsByJobName(@Param("jobName") String jobName, @Param("offset") int offset,@Param("pic")long pic);

	@Query(value = "SELECT * FROM jobs job WHERE job_id IN(:jobId)", nativeQuery = true)
	public List<JobDto> findJobsFromJobIdArray(@Param("jobId") List<Long> jobId);

	@Query("FROM JobDto as job WHERE job.status=:status AND job.personIncharge=:pic")
	public List<JobDto> findAllJobsPerStatus(@Param("status") String status, @Param("pic") long personIncharge);

	@Query("FROM JobDto as job WHERE job.personIncharge=:pic")
	public List<JobDto> getAllInchargeJobs(@Param("pic") long inchargeId);

}