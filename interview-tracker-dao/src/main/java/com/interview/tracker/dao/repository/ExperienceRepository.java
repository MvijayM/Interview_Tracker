package com.interview.tracker.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.interview.tracker.dao.dto.ExperienceDto;

public interface ExperienceRepository extends JpaRepository<ExperienceDto, Long> {
	@Query(value = "DELETE FROM user_experience WHERE user_experience.user_id=:userId", nativeQuery = true)
	@Modifying
	public void deleteAllExperienceOfUser(@Param("userId") long userId);

	@Query(value = "SELECT * FROM user_experience WHERE user_experience.user_id=:userId", nativeQuery = true)
	public List<ExperienceDto> getUserExprience(@Param("userId") long userId);
}
