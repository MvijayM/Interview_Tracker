package com.interview.tracker.dao.entity;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

import com.interview.tracker.dao.dto.RoleDto;
import com.interview.tracker.dao.dto.UserDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data

@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserEntity {

	private long userId;

	private String userName;

	private String password;

	private String email;

	private boolean isEnabled;

	private int verificationOTP;

	private String skills;

	private int totalExperience;

	private String gender;

	private String highestQualification;

	private List<ExperienceEntity> experience;

	private List<RoleEntity> roles;

	public static UserEntity FormEntity(UserDto dto) {
		if (Objects.isNull(dto))
			return null;
		return UserEntity.builder().userId(dto.getUserId()).email(dto.getEmail()).userName(dto.getUserName())
				.password(dto.getPassword()).isEnabled(dto.isEnabled()).verificationOTP(dto.getVerificationOTP())
				.totalExperience(dto.getTotalExperience()).gender(dto.getGender())
				.highestQualification(dto.getHighestQualification()).skills(dto.getSkills())
				.roles(dto.getRoles().stream().map(RoleEntity::formEntity).collect(Collectors.toList())).build();
	}

	public static UserDto FormDto(UserEntity entity) {
		if (Objects.isNull(entity))
			return null;

		UserDto userDto = UserDto.builder().userId(entity.getUserId()).userName(entity.getUserName())
				.password(entity.getPassword()).email(entity.getEmail()).isEnabled(entity.isEnabled())
				.totalExperience(entity.getTotalExperience()).gender(entity.getGender())
				.highestQualification(entity.getHighestQualification()).skills(entity.getSkills())
				.verificationOTP(entity.getVerificationOTP()).build();
		if (!CollectionUtils.isEmpty(entity.getRoles()))
			entity.getRoles().stream().forEach(role -> {
				RoleDto roleDto = RoleEntity.formDto(role);
				userDto.addRole(roleDto);
			});
		if (!CollectionUtils.isEmpty(entity.getExperience())) {
			entity.getExperience().stream().forEach(experience -> {
				userDto.addExperience(ExperienceEntity.formDto(experience));
			});
		}
		return userDto;
	}
}
