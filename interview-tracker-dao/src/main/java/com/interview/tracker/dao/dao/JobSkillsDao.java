package com.interview.tracker.dao.dao;

import java.util.List;

public interface JobSkillsDao {

	public List<String>getSkillsByJobId(long jobId);
}
