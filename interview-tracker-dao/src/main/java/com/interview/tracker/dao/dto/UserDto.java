package com.interview.tracker.dao.dto;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_info")
public class UserDto {

	@Id
	@Column(name = "user_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long userId;

	@Column(unique = true, name = "user_name")
	private String userName;

	@Column(name = "password")
	private String password;

	@Column(unique = true, name = "emailId")
	private String email;

	@Column(name = "isEnabled")
	private boolean isEnabled;

	@Column(name = "verification_otp")
	private int verificationOTP;

	@Column(name = "skills")
	private String skills;

	@Column(name = "total_experience",columnDefinition = "int default 0")
	private int totalExperience;

	@Column(name = "gender")
	private String gender;

	@Column(name = "highest_qualification")
	private String highestQualification;

	@OneToMany(mappedBy = "userProfile", cascade = CascadeType.ALL, targetEntity = ExperienceDto.class)
	@Builder.Default
	private List<ExperienceDto> experience = new ArrayList<>();

	public void addExperience(ExperienceDto expereinceDto) {
		experience.add(expereinceDto);
		expereinceDto.setUserProfile(this);

	}

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = RoleDto.class)
	@Builder.Default
	private List<RoleDto> roles = new ArrayList<>();

	public void addRole(RoleDto role) {
		roles.add(role);
		role.setUser(this);
	}

}
