package com.interview.tracker.dao.dao;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;

import com.interview.tracker.dao.entity.UserEntity;

/**
 * 
 * To handle data access related to users
 * @author vijai
 *
 */
public interface UserDao {

	
	/**
	 * 
	 * @param userId
	 * @return the user details
	 */
	public UserEntity findByUserId(long userId);
	
	
	/**
	 * 
	 * @param userEntity
	 * 
	 */
	public void registerUser(UserEntity userEntity);
	
	/**
	 * 
	 * @param UserName
	 * @return userDetails
	 */
	public UserDetails findByUserName(String UserName);


	public UserEntity findUserByName(String userName);


	public boolean updateOrSave(UserEntity userEntity);


	public UserEntity findUserByEmailId(String emailId);
	
	public List<UserEntity> getAllUsersExcept(String userName, String keyword, int offset);
	
}

