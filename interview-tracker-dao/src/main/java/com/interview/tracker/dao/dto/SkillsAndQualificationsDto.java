package com.interview.tracker.dao.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="job_skills")
public class SkillsAndQualificationsDto {

	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	@Column(name = "Skill_id")
	private long skillId;
	
	@Column(name="Skill")
	private String skill;
	
	
	
	@ManyToOne
	@JoinColumn(name="job_id",referencedColumnName = "job_id")
	private JobDto job;
	
}
