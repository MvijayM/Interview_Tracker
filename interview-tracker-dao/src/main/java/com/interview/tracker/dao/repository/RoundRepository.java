package com.interview.tracker.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.interview.tracker.dao.dto.RoundDto;

public interface RoundRepository extends JpaRepository<RoundDto, Long>{

}
