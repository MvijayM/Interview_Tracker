package com.interview.tracker.dao.entity;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.interview.tracker.dao.dto.JobDto;
import com.interview.tracker.dao.dto.JobResponsibilitiesDto;
import com.interview.tracker.dao.dto.RoundDto;
import com.interview.tracker.dao.dto.SkillsAndQualificationsDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JobEntity {
	private long jobId;

	private String jobName;

	private String jobDescription;

	private String organisation;

	private long personIncharge;

	// TODO replace with enum
	private String status;

	private int numberOfOpenings;

	private int minExperience;

	private int maxExperience;

	private int numOfRounds;
	
	private int minSalary;
	
	private int maxSalary;

	private List<RoundEntity> availableOpenings;

	private List<JobResponsibilitiesEntity> responsibilities;

	private List<SkillsAndQualificationsEntity> skillsAndQualifications;

	public static JobEntity formEntity(JobDto dto) {
		if (Objects.isNull(dto))
			return null;
		return JobEntity.builder().jobId(dto.getJobId()).jobName(dto.getJobName())
				.personIncharge(dto.getPersonIncharge()).status(dto.getStatus()).jobDescription(dto.getJobDescription())
				.numberOfOpenings(dto.getNumberOfOpenings()).minExperience(dto.getMinExperience())
				.maxExperience(dto.getMaxExperience()).numOfRounds(dto.getNumOfRounds())
				.organisation(dto.getOrganisation())
				.minSalary(dto.getMinSalary())
				.maxSalary(dto.getMaxSalary())
				.availableOpenings(
						dto.getRoundsAvailable().stream().map(RoundEntity::formEntity).collect(Collectors.toList()))
				.responsibilities(dto.getResponsiblities().stream().map(JobResponsibilitiesEntity::formEntity)
						.collect(Collectors.toList()))
				.skillsAndQualifications(dto.getSkillsAndQualifications().stream()
						.map(SkillsAndQualificationsEntity::formEntity).collect(Collectors.toList()))
				.build();
	}

	public static JobDto formDto(JobEntity entity) {
		if (Objects.isNull(entity))
			return null;
		JobDto job = JobDto.builder().jobId(entity.getJobId()).jobName(entity.getJobName())
				.personIncharge(entity.getPersonIncharge()).status(entity.getStatus())
				.organisation(entity.getOrganisation()).jobDescription(entity.getJobDescription())
				.minSalary(entity.getMinSalary()).maxSalary(entity.getMaxSalary())
				.numberOfOpenings(entity.getNumberOfOpenings()).minExperience(entity.getMinExperience())
				.maxExperience(entity.getMaxExperience()).numOfRounds(entity.getNumOfRounds()).build();
		entity.getSkillsAndQualifications().stream().forEach(skilEntity -> {
			SkillsAndQualificationsDto skillDto = SkillsAndQualificationsEntity.formDto(skilEntity);
			job.addSkill(skillDto);
		});

		entity.getAvailableOpenings().stream().forEach(round -> {
			RoundDto roundDto = RoundEntity.formDto(round);
			job.addRound(roundDto);
		});

		entity.getResponsibilities().stream().forEach(responsibility -> {
			JobResponsibilitiesDto responsibilityDto = JobResponsibilitiesEntity.formDto(responsibility);
			job.addResponsibility(responsibilityDto);
		});
		return job;
	}
}
