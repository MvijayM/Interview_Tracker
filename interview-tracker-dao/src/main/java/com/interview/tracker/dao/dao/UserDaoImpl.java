package com.interview.tracker.dao.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.CollectionUtils;

import com.interview.tracker.dao.dto.UserDto;
import com.interview.tracker.dao.entity.JobEntity;
import com.interview.tracker.dao.entity.UserEntity;
import com.interview.tracker.dao.repository.UserRepository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserDaoImpl implements UserDao{
	
	private final UserRepository userRepository;
	
	@Override
	public UserEntity findByUserId(long userId) {
		return UserEntity.FormEntity(userRepository.findById(userId).orElse(null));
	}

	@Override
	public void registerUser(UserEntity userEntity) {
		
    	userRepository.save(UserEntity.FormDto(userEntity));
	
	}

	@Override
	public UserDetails findByUserName(String userName) throws UsernameNotFoundException {
		
		List<UserDto> users = userRepository.findByUserName(userName);
        if (CollectionUtils.isEmpty(users)) {
            throw new UsernameNotFoundException("Could not find user");
        }
        UserEntity user = UserEntity.FormEntity(users.get(0));
        List<GrantedAuthority> authorities = user.getRoles().stream().map(roleEntity ->
        	new SimpleGrantedAuthority("ROLE_"+ roleEntity.getRoleName())
        ).collect(Collectors.toList());
        return new org.springframework.security.core.userdetails.User(users.get(0).getUserName(), user.getPassword(),user.isEnabled(),true,true,true,authorities);


	}

	@Override
	public UserEntity findUserByName(String userName) {
		List<UserDto> users = userRepository.findByUserName(userName);
        return UserEntity.FormEntity(users.stream().findFirst().orElse(null));
	}

	@Override
	public boolean updateOrSave(UserEntity userEntity) {
		try {
			userRepository.save(UserEntity.FormDto(userEntity));
			return true;
		} catch(Exception exception) {
			exception.printStackTrace();
			return false;
		}
	}

	@Override
	public UserEntity findUserByEmailId(String emailId) {
		return UserEntity.FormEntity(userRepository.findUserByEmailId(emailId));
	}
	
	public List<UserEntity> getAllUsersExcept(String userName, String keyword, int offset) {
		return userRepository.getAllUsersExcept(userName,keyword,offset).stream().map(UserEntity::FormEntity).collect(Collectors.toList());
	}
}

	
	

