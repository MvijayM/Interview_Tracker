package com.interview.tracker.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.interview.tracker.dao.dto.UserDto;

public interface UserRepository extends JpaRepository<UserDto, Long> {

	List<UserDto> findByUserName(String userName);

	@Query(value = "FROM UserDto as user WHERE user.email=:emailId")
	UserDto findUserByEmailId(@Param("emailId") String emailId);

	@Query(value = "SELECT * FROM user_info as user WHERE user.user_name<>:userName AND user.user_name<>'admin' AND user.user_name LIKE %:keyword% ORDER BY user.user_name  LIMIT 20 OFFSET :offset", nativeQuery = true)
	List<UserDto> getAllUsersExcept(@Param("userName") String userName, @Param("keyword") String keyword,
			@Param("offset") int offSet);
}
