package com.interview.tracker.dao.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.interview.tracker.dao.entity.RoundEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="job_responsibility")
public class JobResponsibilitiesDto {

	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	@Column(name = "Responsibility_id")
	private long ResponsibiltyId;
	
	@Column(name="Responsibility")
	private String Responsibilties;
	
	
	
	@ManyToOne
	@JoinColumn(name="job_id",referencedColumnName = "job_id")
	private JobDto job;
	
}
