package com.interview.tracker.dao.dao;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.interview.tracker.dao.entity.ExperienceEntity;
import com.interview.tracker.dao.repository.ExperienceRepository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ExperienceDaoImpl implements ExperienceDao {

	private final ExperienceRepository expRepo;

	@Override
	public boolean deleteAllExperienceOfUser(long userId) {
		try {
			expRepo.deleteAllExperienceOfUser(userId);
			return true;
		} catch (Exception exception) {
			exception.printStackTrace();
			return false;
		}
	}

	@Override
	public List<ExperienceEntity> getUserExperience(long userId) {
		return expRepo.getUserExprience(userId).stream().map(ExperienceEntity::formEntity).collect(Collectors.toList());
	}

}
