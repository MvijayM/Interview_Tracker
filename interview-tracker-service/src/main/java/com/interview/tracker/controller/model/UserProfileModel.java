package com.interview.tracker.controller.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class UserProfileModel {

	private String userName;

	private String gender;

	private String roles;

	private String email;

	private String highestQualification;

	private int totalExp;

	private List<String> skills;

	private List<IndividualExperience> companyExpList;

}
