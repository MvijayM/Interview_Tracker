package com.interview.tracker.controller.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AssignRolesUserRequestModel {

	private String keyword;
	private int offset;
	private String userName;
}
