package com.interview.tracker.controller.model;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OTPRequestModel {

	private String userName;
	private String email;
	private int otpEntered;
}
