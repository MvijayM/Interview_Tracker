package com.interview.tracker.controller.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CandidateChartResponse {
   
	 private int selected;
	 
	 private int inProgress;
	 
	 private int rejected;
	 
	 private int applied;
	 
}
