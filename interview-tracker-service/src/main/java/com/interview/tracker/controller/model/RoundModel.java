package com.interview.tracker.controller.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class RoundModel {

	private String roundName;
	private int roundNo;
	private String roundDescription;

}
