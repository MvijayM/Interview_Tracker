package com.interview.tracker.controller.model;

import java.util.List;
import java.util.Objects;

import com.interview.tracker.dao.entity.CandidateEntity;
import com.interview.tracker.dao.entity.JobEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class candidateAppliedJobsResponse {

	private String candidateName;

	private String jobName;

	private String jobShortDescription;

	private String Status;

	private List<RoundModel> rounds;

	private Number roundNumber;

	/**
	 * Used to convert the jobEntity from DAO layer to the view response
	 */
	public static candidateAppliedJobsResponse convertToResponse(CandidateEntity candidateEntity, JobEntity jobEntity) {
		if (Objects.nonNull(candidateEntity) && Objects.nonNull(jobEntity)) {
			candidateAppliedJobsResponse response = new candidateAppliedJobsResponse();
			response.setJobName(jobEntity.getJobName());
			response.setCandidateName(candidateEntity.getStatus());
			response.setStatus(candidateEntity.getStatus());
			response.setRoundNumber(candidateEntity.getRoundNumber());
			response.setJobShortDescription(jobEntity.getJobDescription());
			return response;
		}
		return null;
	}
}
