package com.interview.tracker.controller;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.interview.tracker.controller.model.ApiResultModel;
import com.interview.tracker.controller.model.AssignRolesUserRequestModel;
import com.interview.tracker.controller.model.AuthenticationRequestModel;
import com.interview.tracker.controller.model.AuthenticationResponseModel;
import com.interview.tracker.controller.model.OTPRequestModel;
import com.interview.tracker.controller.model.RolesResponseModel;
import com.interview.tracker.controller.model.UserProfileModel;
import com.interview.tracker.controller.model.UserResponseModel;
import com.interview.tracker.dao.entity.UserEntity;
import com.interview.tracker.security.JWTUtil;
import com.interview.tracker.security.UserNotVerfiedException;
import com.interview.tracker.service.UserService;

@RestController
@RequestMapping(value = "/api/user")
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JWTUtil jwtUtil;

	@PostMapping(value = "/noAuth/register")
	public ApiResultModel RegisterUser(@RequestBody UserEntity user) {
		try {
			return userService.registerUser(user);
		} catch (IllegalArgumentException exception) {
			return new ApiResultModel(false, "Error Sending Mail");
		}
	}

	@PostMapping(value = "/noAuth/login")
	public AuthenticationResponseModel performLogin(@RequestBody AuthenticationRequestModel request) {
		final UserDetails userDetails = userDetailsService.loadUserByUsername(request.getUserName());
		Authentication authentication = null;
		try {
			authentication = authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(request.getUserName(), request.getPassword()));
		} catch (BadCredentialsException exception) {
			return AuthenticationResponseModel.builder().authResult(false).build();
		} catch (UserNotVerfiedException verficationException) {
			return AuthenticationResponseModel.builder().authResult(true).verificationResult(false).build();
		}
		final String jwt = jwtUtil.generateToken(userDetails);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		return AuthenticationResponseModel.builder().jwt(jwt).userResponse(this.getUserByName(request.getUserName()))
				.authResult(true).verificationResult(true).build();

	}

	@GetMapping(value = "/userById/{userId}")
	public UserEntity getUserById(@PathVariable("userId") long userId) {
		return UserEntity.FormEntity(userService.findByUSerId(userId));
	}

	@GetMapping(value = "/getUserByName/{userName}")
	public UserResponseModel getUserByName(@PathVariable("userName") String userName) {
		return userService.findUserByName(userName);
	}

	@PostMapping(value = "/noAuth/verifyOtp")
	public boolean verifyOtp(@RequestBody OTPRequestModel request) {
		return userService.verifyOtp(request);
	}

	@PostMapping(value = "/noAuth/resendOtp")
	public boolean resendOtp(@RequestBody OTPRequestModel request) {
		try {
			return userService.resendOtp(request);
		} catch (IllegalArgumentException exception) {
			return false;
		}
	}

	@GetMapping(value = "/noAuth/checkUserNameAvailability/{nameToCheck}")
	public boolean checkUserNameAvailability(@PathVariable("nameToCheck") String nameToCheck) {
		return Objects.isNull(userService.findUserByName(nameToCheck));
	}

	@GetMapping(value = "/noAuth/checkEmailAvailability/{email}")
	public boolean checkEmailAvailability(@PathVariable("email") String email) {
		return Objects.isNull(userService.findUserByEmailId(email));
	}

	@PostMapping(value = "/admin/getAllUsersExcept")
	public List<UserResponseModel> getAllUsersExcept(@RequestBody AssignRolesUserRequestModel request) {
		return userService.findAllUsersExcept(request);
	}

	@GetMapping(value = "/admin/getUserRoles/{userName}")
	public RolesResponseModel getUserRoles(@PathVariable("userName") String userName) {
		return userService.getUserRoles(userName);
	}

	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping(value = "/admin/updateRoles")
	public ApiResultModel updateRoles(@RequestBody UserResponseModel request) {
		return userService.updateRoles(request);
	}

	@PostMapping(value = "/saveProfile")
	public ApiResultModel saveProfile(@RequestBody() UserProfileModel userProfileModel) {
		try {
			return userService.saveProfile(userProfileModel);
		} catch (Exception e) {
			return new ApiResultModel(false, "Profile not Updated");
		}
	}

	@GetMapping(value = "/getUserProfile/{userName}")
	public UserProfileModel getUserProfile(@PathVariable("userName") String userName) {
		return userService.getUserProfile(userName);
	}
}
