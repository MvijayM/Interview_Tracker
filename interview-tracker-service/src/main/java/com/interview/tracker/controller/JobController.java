package com.interview.tracker.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.interview.tracker.controller.model.AllJobs;
import com.interview.tracker.controller.model.ApiResultModel;
import com.interview.tracker.controller.model.CandidateChartResponse;
import com.interview.tracker.controller.model.JobGroupStatusModel;
import com.interview.tracker.controller.model.JobOpeningStatus;
import com.interview.tracker.controller.model.JobsResponse;
import com.interview.tracker.controller.model.RegisterNewJobRequest;
import com.interview.tracker.dao.dao.UserDao;
import com.interview.tracker.dao.entity.CandidateEntity;
import com.interview.tracker.dao.entity.UserEntity;
import com.interview.tracker.service.CandidateService;
import com.interview.tracker.service.JobService;
import com.interview.tracker.service.UserService;

@RestController
@RequestMapping(value = "/api/job")
public class JobController {

	@Autowired
	private JobService jobService;

	@Autowired
	private CandidateService candidateService;
	
	@Autowired
	private UserDao userService;

	/**
	 * 
	 * @param newJobRequest
	 * @return Response -> Success or failure
	 */

	@PostMapping(value = "/addNewJob")
	public ApiResultModel insertData(@RequestBody RegisterNewJobRequest newJobRequest) {

		return jobService.addNewJob(newJobRequest);
	}

	/**
	 * 
	 * @param jobId
	 * @return jobs by jobId
	 */

	@GetMapping(value = "/getJobById/{jobId}")
	public JobsResponse getJobByJobId(@PathVariable("jobId") long jobId) {
		return jobService.findJobById(jobId);
	}

	/**
	 * 
	 * @param candidateName
	 * @return all jobs applied by a candidate.
	 */
	@GetMapping(value = "/getAllJobs/{candidateName}")
	public AllJobs findAllTheJobsApplied(@PathVariable("candidateName") String candidateName) {
		List<JobsResponse> appliedJobs = jobService.findAllJobsByCandidateName(candidateName);
		List<JobsResponse> unAppliedJobs = jobService.findAllUnAppliedJobs(candidateName);
		return AllJobs.builder().appliedJobs(appliedJobs).unAppliedJobs(unAppliedJobs).build();
	}

	@GetMapping(value = "/getJobsByPic/{personInCharge}")
	public List<JobsResponse> findAllJobsByPersonInCharge(@PathVariable("personInCharge") String personIncharge) {
		List<JobsResponse> jobsByPic = jobService.findJobsByPic(personIncharge);
		return jobsByPic;
	}

	@GetMapping(value = "/getJobsLikeJobName")
	public List<JobsResponse> findAllJobsLikeJobName(@RequestParam("jobName") String jobName,
			@RequestParam("offset") int offset,@RequestParam("pic")String pic) {
		UserEntity user = userService.findUserByName(pic);
		List<JobsResponse> jobsLikeJobName = jobService.findJobLikeJobName(jobName, offset,user.getUserId());
		return jobsLikeJobName;
	}

	@GetMapping(value = "/hr/getJobsGroupedByStatus/{userName}")
	public JobGroupStatusModel getJobsGroupedByStatus(@PathVariable("userName") String userName) {
		return jobService.getJobsGroupedByStatus(userName);
	}

	@GetMapping(value = "/getJobsByCandidateName/{candidateName}")
	public CandidateChartResponse findAllJobsByCandidateName(@PathVariable("candidateName") String candidateName) {
		List<CandidateEntity> candidate = candidateService.findByCandidateName(candidateName);

		Map<String, List<CandidateEntity>> groupMap = candidate.stream()
				.collect(Collectors.groupingBy(localCandidate -> localCandidate.getStatus()));

		return CandidateChartResponse.builder().selected(groupMap.getOrDefault("SELECTED", new ArrayList<>()).size())
				.applied(groupMap.getOrDefault("APPLIED", new ArrayList<>()).size())
				.rejected(groupMap.getOrDefault("REJECTED", new ArrayList<>()).size())
				.inProgress(groupMap.getOrDefault("IN PROGRESS", new ArrayList<>()).size()).build();
	}

	@GetMapping(value = "/hr/getJobOpeningStatus/{jobId}")
	public JobOpeningStatus getJobOpeningStatus(@PathVariable("jobId") long jobId) {
		return jobService.getJobOpeningStatus(jobId);
	}

}
