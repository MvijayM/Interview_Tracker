package com.interview.tracker.controller.model;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

import com.interview.tracker.dao.entity.JobEntity;
import com.interview.tracker.dao.entity.JobResponsibilitiesEntity;
import com.interview.tracker.dao.entity.RoundEntity;
import com.interview.tracker.dao.entity.SkillsAndQualificationsEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor

public class RegisterNewJobRequest {

	private String jobName;
	private String jobShortDescription;
	private int minExperience;
	private int maxExperience;
	private int numberOfOpenings;
	private String organisation;
	private String personInCharge;
	private String status;
	private int minSalary;
	private int maxSalary;
	private List<String> responsibilities;
	private List<String> skillsAndQualification;
	private List<RoundModel> rounds;

	/**
	 * used to convert the request object to Entity for DAO layer
	 */
	public JobEntity convertToEntity() {
		JobEntity newJobEntity = new JobEntity();
		// add job related details
		newJobEntity.setJobName(getJobName());
		newJobEntity.setJobDescription(getJobShortDescription());
		newJobEntity.setMinExperience(getMinExperience());
		newJobEntity.setMaxExperience(getMaxExperience());
		newJobEntity.setNumberOfOpenings(getNumberOfOpenings());
		newJobEntity.setOrganisation(getOrganisation());
	    newJobEntity.setMinSalary(getMinSalary());
	    newJobEntity.setMaxSalary(getMaxSalary());
		newJobEntity.setStatus("OPEN");
		// add responsibilities
		if (!CollectionUtils.isEmpty(getResponsibilities())) {
			newJobEntity.setResponsibilities(getResponsibilities().stream()
					.map(responsibility -> JobResponsibilitiesEntity.builder().responsiblities(responsibility).build())
					.collect(Collectors.toList()));
		}
		// add skills & qualification
		if (!CollectionUtils.isEmpty(getSkillsAndQualification())) {
			newJobEntity.setSkillsAndQualifications(getSkillsAndQualification().stream()
					.map(skill -> SkillsAndQualificationsEntity.builder().skill(skill).build())
					.collect(Collectors.toList()));
		}

		// add rounds
		if (!CollectionUtils.isEmpty(getRounds())) {
			AtomicInteger index = new AtomicInteger();
			newJobEntity.setAvailableOpenings(getRounds().stream()
					.map(round -> RoundEntity.builder().roundName(round.getRoundName())
							.roundDescription(round.getRoundDescription()).roundNo(index.incrementAndGet()).build())
					.collect(Collectors.toList()));
		}

		newJobEntity.setNumOfRounds(rounds.size());
		return newJobEntity;
	}
}
