package com.interview.tracker.controller.model;



import java.util.Date;

import com.interview.tracker.dao.entity.CandidateEntity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor

public class CandidateRequest {

	 private long jobId;
	 
	 private long candidateId;
	 
	 private int roundNumber;
	 
	 private String candidateName;
	 
	 private String emailId;
	 
	 private String status;
	 
	 private Date sheduledDate;
	 
	 private String time;
	 
	 
	 /**
		 * used to convert the request object to Entity for DAO layer
		 * */
		public CandidateEntity convertToEntity() {
			CandidateEntity candidateEntity = new CandidateEntity();
			// add job related details
			candidateEntity.setJobId(getJobId());
			candidateEntity.setCandidateId(getCandidateId());
			candidateEntity.setCandidateName(getCandidateName());
			candidateEntity.setEmailId(getEmailId());
			candidateEntity.setRoundNumber(getRoundNumber());
			candidateEntity.setStatus(getStatus());	
			candidateEntity.setSheduledDate(getSheduledDate());
			candidateEntity.setTime(getTime());
			return candidateEntity;
		}
	}


