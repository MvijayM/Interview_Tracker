package com.interview.tracker.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.interview.tracker.controller.model.ApiResultModel;
import com.interview.tracker.controller.model.JobGroupStatusModel;
import com.interview.tracker.controller.model.JobOpeningStatus;
import com.interview.tracker.controller.model.JobsResponse;
import com.interview.tracker.controller.model.RegisterNewJobRequest;
import com.interview.tracker.dao.dao.CandidateDao;
import com.interview.tracker.dao.dao.JobDao;
import com.interview.tracker.dao.dao.JobSkillsDao;
import com.interview.tracker.dao.dao.UserDao;
import com.interview.tracker.dao.entity.CandidateEntity;
import com.interview.tracker.dao.entity.JobEntity;
import com.interview.tracker.dao.entity.UserEntity;

@Service
public class JobService {

	@Autowired
	private JobDao jobDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private CandidateDao candidateDao;

	@Autowired
	private JobSkillsDao jobSkillsDao;

	/**
	 * used to find a job by using the jobId
	 * 
	 * @param jobId, the job id for which the details is needed
	 * @return the job details
	 */
	public JobsResponse findJobById(long jobId) {

		// fetch the job
		JobEntity fetchedJob = jobDao.findJobById(jobId);
		// convert to response
		return JobsResponse.convertToResponse(fetchedJob);
	}

	/**
	 * used to add a new job
	 * 
	 * @param newJob, the details of the new job to be added
	 * 
	 * @return true if job added successfully, false if job addition failed
	 */
	public ApiResultModel addNewJob(RegisterNewJobRequest newJob) {

		// To set Person in charge
		UserEntity userDetails = userDao.findUserByName(newJob.getPersonInCharge());
		// convert the request
		JobEntity entity = newJob.convertToEntity();
		entity.setPersonIncharge(userDetails.getUserId());
		boolean result = jobDao.addNewJob(entity);
		return result ? new ApiResultModel(true, "success") : new ApiResultModel(false, "failure");

	}

	/**
	 * used to fetch all the jobs that are applied by the candidate
	 * 
	 * @param candidateId, the id of the candidate for whom the result is needed
	 */
	public List<JobsResponse> findAllJobsByCandidateName(String userName) {
		// fetch the jobs applied
		List<JobEntity> fetchedJobs = jobDao.findAllJobsByCandidateName(userName);
		if (CollectionUtils.isEmpty(fetchedJobs)) {
			return Collections.emptyList();
		}
		// convert to response
		return fetchedJobs.stream().map(x -> {
			JobsResponse ans = JobsResponse.convertToResponse(x);
			ans.setAppliedStatus("Applied");
			return ans;
		}).collect(Collectors.toList());
	}

	/**
	 * used to fetch all the jobs that the candidate can apply at present
	 * 
	 * @param candidateId, the id of the candidate for whom the result is needed
	 */
	public List<JobsResponse> findAllUnAppliedJobs(String userName) {
		// fetch the jobs that can be applied
		List<JobEntity> fetchedJobs = jobDao.findAllUnAppliedJobs(userName);
		if (CollectionUtils.isEmpty(fetchedJobs)) {
			return Collections.emptyList();
		}
		// convert to response
		return fetchedJobs.stream().map(x -> {
			JobsResponse ans = JobsResponse.convertToResponse(x);
			ans.setAppliedStatus("UnApplied");
			return ans;
		}).collect(Collectors.toList());
	}

	/**
	 * used to fetch all jobs by person in charge
	 * 
	 * @param personInCharge
	 * @return
	 */
	public List<JobsResponse> findJobsByPic(String personIncharge) {
		UserEntity userDetails = userDao.findUserByName(personIncharge);

		List<JobEntity> fetchedJobs = jobDao.findJobsByPersonInCharge(userDetails.getUserId());
		if (CollectionUtils.isEmpty(fetchedJobs)) {
			return Collections.emptyList();
		}
		return fetchedJobs.stream().map(x -> {
			JobsResponse ans = JobsResponse.convertToResponse(x);
			return ans;
		}).collect(Collectors.toList());
	}

	public List<JobsResponse> findJobLikeJobName(String jobName, int offset,long pic) {
		List<JobEntity> fetchedJobs = jobDao.findJobsLikeJobName(jobName, offset,pic);
		if (CollectionUtils.isEmpty(fetchedJobs)) {
			return Collections.emptyList();
		}
		return fetchedJobs.stream().map(x -> {
			JobsResponse ans = JobsResponse.convertToResponse(x);
			return ans;
		}).collect(Collectors.toList());
	}

	/**
	 * used to fetch all the responsible jobs with status and its corresponding
	 * count
	 * 
	 * @param userName
	 */
	public JobGroupStatusModel getJobsGroupedByStatus(String userName) {
		UserEntity userDetails = userDao.findUserByName(userName);
		List<JobsResponse> jobs = jobDao.getAllInchargeJobs(userDetails.getUserId()).stream()
				.map(JobsResponse::convertToResponse).collect(Collectors.toList());
		if (!CollectionUtils.isEmpty(jobs)) {
			Map<String, Integer> groupedMap = new HashMap<>();
			jobs.stream().forEach(job -> {
				groupedMap.put(job.getJobStatus(), groupedMap.getOrDefault(job.getJobStatus(), 0) + 1);
			});
			return JobGroupStatusModel.builder().closed(groupedMap.getOrDefault("CLOSED", 0))
					.open(groupedMap.getOrDefault("OPEN", 0)).build();
		}
		return JobGroupStatusModel.builder().build();
	}

	public JobOpeningStatus getJobOpeningStatus(long jobId) {
		JobEntity job = jobDao.findJobById(jobId);
		List<CandidateEntity> selectedCandidates = candidateDao.getSelectedCandidatesByJobId(jobId);
		return JobOpeningStatus.builder().filled(selectedCandidates.size()).remaining(job.getNumberOfOpenings())
				.build();
	}

	public String getMatchingPercentage(long jobId, String userName) {
		double percentageExperience = 0;
		List<String> jobSkills = jobSkillsDao.getSkillsByJobId(jobId);
		UserEntity user = userDao.findUserByName(userName);
		String candidateSkills = user.getSkills();
		int candidateExperience = user.getTotalExperience();
		JobEntity jobs = jobDao.findJobById(jobId);
		int jobMinExpeirence = jobs.getMinExperience() * 12;
		List<String> candidateSkillsList = Stream
				.of(Objects.nonNull(candidateSkills) ? candidateSkills.split(",") : new String[0])
				.collect(Collectors.toList());
		List<String> matchedSkills = jobSkills.stream().filter(candidateSkillsList::contains)
				.collect(Collectors.toList());
		int matchedSkillsCount = matchedSkills.size();
		double percentMatchOutOfhundred = (matchedSkillsCount * 100) / jobSkills.size();
		double percentOutOfseventy = (70 * percentMatchOutOfhundred) / 100;
		if (candidateExperience >= jobMinExpeirence) {
			percentageExperience = 30;
		} else {
			int diff = Math.abs(candidateExperience - jobMinExpeirence);
			if (diff <= 24 && diff >= 12) {
				percentageExperience = 10;
			} else if (diff <= 12) {
				percentageExperience = 20;
			}
		}
		return String.valueOf(percentOutOfseventy + percentageExperience);
	}

}
