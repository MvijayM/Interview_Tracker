package com.interview.tracker.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.interview.tracker.dao.config.DaoConfig;

@Configuration
@Import({
	DaoConfig.class	
})
public class ServiceConfig {

}
