package com.interview.tracker.security;

import org.springframework.security.core.AuthenticationException;

public class UserNotVerfiedException extends AuthenticationException {

	public UserNotVerfiedException(String message) {
		super(message);
	}
}
