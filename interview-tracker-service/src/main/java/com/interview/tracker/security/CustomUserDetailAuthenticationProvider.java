package com.interview.tracker.security;

import java.util.Objects;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.interview.tracker.dao.entity.UserEntity;
import com.interview.tracker.service.UserService;

public class CustomUserDetailAuthenticationProvider implements AuthenticationProvider{

	
	private final UserService userService;
	
	private final BCryptPasswordEncoder passwordEncoder;
	
	private static CustomUserDetailAuthenticationProvider singleTonInstance;
	
	public static CustomUserDetailAuthenticationProvider getInstance(UserService userService, BCryptPasswordEncoder passwordEncoder) {
		if(singleTonInstance==null) {
			singleTonInstance = new  CustomUserDetailAuthenticationProvider(userService, passwordEncoder);
		}
		return singleTonInstance;
	}
	
	private CustomUserDetailAuthenticationProvider(UserService userService, BCryptPasswordEncoder passwordEncoder) {
		super();
		this.userService = userService;
		this.passwordEncoder = passwordEncoder;
	}
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String userName = authentication.getName();
		if(Objects.isNull(authentication.getCredentials())) {
			return null;
		}
		String password = authentication.getCredentials().toString();
		UserDetails user = userService.findByUserName(userName);
		if(user==null) {
			throw new UsernameNotFoundException("User :"+ userName +"Doesn't Exist");
		}
		if(passwordEncoder.matches(password, user.getPassword())) {
			if(!user.isEnabled()) {
				throw new UserNotVerfiedException("Email Not Verified");
			}
			return new UsernamePasswordAuthenticationToken(
					user, null, user.getAuthorities());
		} else {
			user = null;
			throw new BadCredentialsException("User : "+ userName + "Password is incorrect");
		}
		
		
		
		
	}
	

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
