package com.interview.tracker.security;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.interview.tracker.controller.model.AuthenticationResponseModel;

@Component
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {
		if(authException instanceof DisabledException) {
			AuthenticationResponseModel returnObject = AuthenticationResponseModel.builder().authResult(true).verificationResult(false).build();
			ServletOutputStream outputStream = response.getOutputStream();
			response.setContentType("application/json;charset=UTF-8");
			outputStream.print(new Gson().toJson(returnObject));
			outputStream.flush();
		}else {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Bad credentials");
		}
	}


}
