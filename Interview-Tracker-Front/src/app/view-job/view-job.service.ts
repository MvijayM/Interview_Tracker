import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppConfig } from '../core/app-config';
import { LocalStorageUtil } from '../core/auth/local-storage-util';
import { AllJobs, CandidateAppliedJobs } from './view-job.component';

@Injectable({
  providedIn: 'root'
})
export class ViewJobService {
  viewAppliedJob() {
    return this._http.get<CandidateAppliedJobs>(AppConfig.API_BASE_PATH + "/api/candidate/getAppliedJobs/" + LocalStorageUtil.getItem("userName"));
  }

  baseUrl: string = AppConfig.API_BASE_PATH + '/api/candidate'
  constructor(private _http: HttpClient) { }

  applyJob(candidateName: string, jobId: number): Observable<any> {
    return this._http.post(this.baseUrl + '/applyJob?candiateName=' + candidateName + '&jobId=' + jobId, {});
  }

  viewJob(): Observable<any> {
    return this._http.get<AllJobs>(AppConfig.API_BASE_PATH + "/api/job/getAllJobs/" + LocalStorageUtil.getItem("userName"));
  }
}
