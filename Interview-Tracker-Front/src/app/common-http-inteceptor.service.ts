import { LocalStorageUtil } from './core/auth/local-storage-util';
import { LoadingService } from './common-component/loading-service';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, map, finalize } from 'rxjs/operators'
import { Router } from '@angular/router';
import { ToasterService } from './toaster.service';
import { DialogData } from './common-component/confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { InformationDisplayDialogComponent } from './common-component/information-display-dialog/information-display-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class CommonHttpInteceptorService implements HttpInterceptor {

  constructor(private _router: Router, private _toaster: ToasterService, private loadingService: LoadingService, private dialog: MatDialog) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (LocalStorageUtil.getItem('jwtToken')) {
      req = req.clone({
        setHeaders: {
          Authorization: 'Bearer ' + LocalStorageUtil.getItem('jwtToken')
        }
      })
    }
    this.loadingService.loadingSubject.next(true);
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
          this._toaster.error('Client Error Sorry for the inconvenience');
        } else {
          console.log(req.headers);
            if(req.headers.has('loginRequest') && error.status === 401) {
              return throwError(error);
            } else {
              if(error.status === 401) {
                this._toaster.info('Session Timeout! Please login to continue.');
                LocalStorageUtil.clearAll();
                this._router.navigate(['/login']);
              } else if( error.status === 403) {
                this.dialog.open(InformationDisplayDialogComponent,{
                  width:'300px',
                  data:{
                    content: "Sorry you don't have authority to view this page.",
                    type: 'warn'
                  }
                });
              } else if (error.status === 400 || error.status === 404) {
                this._toaster.error('Invalid Request!');
              } else if (error.status === 503 || error.status === 504) {
                this._toaster.error('Server Unavailale. Try After Sometime.');
              } else if(error.status === 500) {
                this._toaster.error('Server Error Sorry for the inconvenience.');
              } else {
                return throwError(error);
              }
          }
        }
        return throwError(errorMessage);
      }), finalize(() => {
        this.loadingService.loadingSubject.next(false);
      }));
  }
}