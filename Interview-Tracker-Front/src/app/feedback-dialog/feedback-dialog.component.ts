import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-feedback-dialog',
  templateUrl: './feedback-dialog.component.html',
  styleUrls: ['./feedback-dialog.component.css']
})
export class FeedbackDialogComponent implements OnInit {
  feedback:string;
  feedbackform: FormGroup;
  
  constructor(private formBuilder: FormBuilder,public dialogRef: MatDialogRef<FeedbackDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {feedback}) { }

  ngOnInit() {
    this.feedbackform = this.formBuilder.group({
      feedback: ['', [Validators.required, Validators.minLength(20),Validators.maxLength(250)]]
    });
  }


  sendResponse(response: boolean) {
    this.dialogRef.close({result:response,data:this.feedback});
  }

}
