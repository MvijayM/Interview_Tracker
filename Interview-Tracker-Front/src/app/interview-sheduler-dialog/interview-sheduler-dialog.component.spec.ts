import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterviewShedulerDialogComponent } from './interview-sheduler-dialog.component';

describe('InterviewShedulerDialogComponent', () => {
  let component: InterviewShedulerDialogComponent;
  let fixture: ComponentFixture<InterviewShedulerDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterviewShedulerDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterviewShedulerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
