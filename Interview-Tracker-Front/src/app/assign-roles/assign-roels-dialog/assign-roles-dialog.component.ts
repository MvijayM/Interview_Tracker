import { UserRolesReponseModel } from '../models/assign-roles.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-assign-roles-dialog',
  templateUrl: './assign-roles-dialog.component.html',
  styleUrls: ['./assign-roles-dialog.component.css'],  
})
export class AssignRolesDialogComponent {
  availableRoles: string[] = ['ADMIN','HR','CANDIDATE'];
  constructor(public dialogRef: MatDialogRef<AssignRolesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserRolesReponseModel) {
  }

  changeValue(event) {
    this.data.roleName = event.value;
    console.log(this.data.roleName);
    console.log(event);
  }
  onAction(action: boolean) {
    this.dialogRef.close({result:action,roles:this.data});
  }

}
