import { MatRadioModule } from '@angular/material/radio';
import { AppConfig } from './../core/app-config';
import { RolesAuthGuard } from './../core/auth/guard/roles-auth-guard';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AssignRolesService } from './assign-roles.service';
import { SearchPageModule } from './../common-component/search-page/search-page.module';
import { MatTableModule } from '@angular/material/table';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssignRolesComponent } from './assign-roles.component';
import { AssignRolesDialogComponent } from './assign-roels-dialog/assign-roles-dialog.component';


const routes: Routes = [{path:'',component:AssignRolesComponent,canActivate: [RolesAuthGuard]}]
@NgModule({
  declarations: [AssignRolesComponent],
  imports: [
    CommonModule,
    MatRadioModule,
    MatTableModule,
    SearchPageModule,    
    MatButtonModule,
    RouterModule.forChild(routes)
  ],
  entryComponents:[AssignRolesDialogComponent],
  providers:[AssignRolesService]
})
export class AssignRolesModule { }
