import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private _noOfOpenings: number;
  
  get noOfOpenings() : number {
    return this._noOfOpenings;
  }

  set noOfOpenings(openings: number) {
    this._noOfOpenings = openings;
  }
  
  constructor() { }

  

}
