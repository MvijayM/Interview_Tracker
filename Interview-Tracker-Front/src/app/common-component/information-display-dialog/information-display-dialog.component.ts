import { DialogData } from './../confirmation-dialog/confirmation-dialog.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-information-display-dialog',
  templateUrl: './information-display-dialog.component.html',
  styleUrls: ['./information-display-dialog.component.css']
})
export class InformationDisplayDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<InformationDisplayDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit() {
  }

}
