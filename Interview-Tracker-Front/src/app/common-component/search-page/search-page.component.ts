
import { MatTableDataSource } from '@angular/material/table';
import { SearchApi } from './api/search-api';
import { SearchColumnDef, GridActionDef } from './models/search-column-def';
import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit, Output, OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject, fromEvent, Subscription } from 'rxjs';
import { debounceTime, map, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { EventEmitter } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';

export class ActionModel<T> {
  action:string;
  data:T;
}
export class MultiRowActionModel<T> {
  action:string;
  data:T[];
}
@Component({
  selector: 'custom-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css']
})
export class SearchPageComponent<T> implements OnInit, AfterViewInit, OnDestroy {
  @Input() displayColumnDefenition: SearchColumnDef[];
  dataSource: MatTableDataSource<any> = new MatTableDataSource([]);
  @Input() api: SearchApi<T>;
  @Input() placeholderText: string = 'Search';
  @Input() refreshGrid: BehaviorSubject<boolean>;
  @Input() rowActions: GridActionDef[];
  @Input() selectable: boolean = false;
  selection = new SelectionModel<T>(true, []);
  @Output() rowClick: EventEmitter<T> = new EventEmitter<T>();
  @Output() onRowAction: EventEmitter<ActionModel<T>> = new EventEmitter<ActionModel<T>>();
  @Output() onMultiRowAction: EventEmitter<MultiRowActionModel<T>> = new EventEmitter<MultiRowActionModel<T>>();
  column
  private keyword: string = '';
  columnIds: string[] = [];

  @ViewChild('filter') filter: ElementRef;
  @ViewChild('gridContainer') gridContainer: ElementRef;
  $subscription: Subscription;
  constructor() {
  }
  onMultiRowActionHandler(actionId: string) {
    this.onMultiRowAction.emit({action:actionId,data:this.selection.selected});
  }

  ngOnInit() {
    this.getDisplayColumns();
    if(this.api){
    this.api.search('', 0).subscribe(data => {
      this.dataSource.data = data;
    });
  }
    if (this.refreshGrid) {
      this.$subscription = this.refreshGrid.subscribe(doRefresh => {
        if (doRefresh) {
          this.api.search(this.keyword, 0).subscribe(data => {
            this.dataSource.data = data;
            this.selection.clear();
          });
        }
      });
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected == numRows;
  }
  
  selectAll() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }
  ngOnDestroy() {
    if(this.$subscription) this.$subscription.unsubscribe();
  }

  onAction(actionId:string,data: T) {
    this.onRowAction.emit({action:actionId,data:data});
  }

  private getDisplayColumns() {
    if (this.displayColumnDefenition) {
      if(this.selectable)
        this.columnIds.push('select');
      this.displayColumnDefenition.forEach(columnDef => this.columnIds.push(columnDef.columnId));
      if(this.rowActions)
        this.columnIds.push('action');
    }
  }

  onRowClick(row, event) {
    if (!event.target.classList?.contains('notClickable'))
      this.rowClick.emit(row);
  }

  ngAfterViewInit() {
    fromEvent(this.filter.nativeElement, "keyup").pipe(debounceTime(1000),
      map((event: Event) => (event.target as HTMLInputElement).value),
      distinctUntilChanged(),
      switchMap(value => this.searchValue(value))).subscribe(result => {
        this.dataSource.data = result;
        this.selection.clear();
      });
    fromEvent(this.gridContainer.nativeElement, 'scroll').pipe(
      map((event: Event) => (event.target as HTMLDivElement).scrollHeight),
      switchMap(scrollHeight => {
        if (this.gridContainer.nativeElement.scrollTop === (this.gridContainer.nativeElement.scrollHeight - this.gridContainer.nativeElement.offsetHeight)) {
          return this.onScroll();
        }
        return [];
      })).subscribe(fetchedresult => {
        this.dataSource.data = this.dataSource.data.concat(fetchedresult);
      });
  }

  private searchValue(keyword: string): Observable<any[]> {
    this.keyword = keyword;
    return this.api.search(keyword, 0);
  }

  onScroll() {
    return this.api.search(this.keyword, this.dataSource.data.length);
  }


}
