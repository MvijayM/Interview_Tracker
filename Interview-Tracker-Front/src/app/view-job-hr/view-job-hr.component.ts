import { Component, OnInit } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppConfig } from '../core/app-config';
import { LocalStorageUtil } from '../core/auth/local-storage-util';
import { JobResponseModel } from './JobResponseModel';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { SearchColumnDef } from '../common-component/search-page/models/search-column-def';
import { SearchApi } from '../common-component/search-page/api/search-api';
import { SearchPageComponent } from '../common-component/search-page/search-page.component';
import { Observable, BehaviorSubject } from 'rxjs';
import { SharedService } from '../common-component/shared.service';



@Component({
  selector: 'app-view-job-hr',
  templateUrl: './view-job-hr.component.html',
  styleUrls: ['./view-job-hr.component.css']
})
export class ViewJobHrComponent implements OnInit {

  displayColumnDefenition: SearchColumnDef[] = [
    { columnId: 'jobName', columnName: 'Job Name' },
    { columnId: 'organisation', columnName: 'Organisation' },
    { columnId: 'jobShortDescription', columnName: 'Job Description' },
    { columnId: 'numberOfOpenings', columnName: 'No Of Openings' }
  ];
  refreshGrid: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  api: SearchApi<JobResponseModel>
  placeholderText: string = "Search for Job Name"

  constructor(private _http: HttpClient, private router: Router, private route: ActivatedRoute, private sharedService: SharedService) { }
  ngOnInit() {
    this.api = new viewHrJobApi(this._http);
    this.refreshGrid.next(true)
  }
  onRowClick(event) {
    this.sharedService.noOfOpenings = event.numberOfOpenings;
    this.router.navigate(['/home/viewcandidatehr'], {
      relativeTo: this.route,
      queryParams: { jobid: event.jobId }
    })
  }
}

export class viewHrJobApi implements SearchApi<JobResponseModel>{
  constructor(private _http: HttpClient) { }

  private jobsUrl = AppConfig.API_BASE_PATH + "/api/job/getJobsLikeJobName/"
  search(keyword: string, offSet: number): Observable<JobResponseModel[]> {


    return this._http.get<JobResponseModel[]>(this.jobsUrl + "?jobName=" + keyword  +"&pic="+LocalStorageUtil.getItem('userName') +"&offset=" + offSet);

  }
}
