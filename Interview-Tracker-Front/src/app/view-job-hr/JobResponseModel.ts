export class JobResponseModel{
    jobName:string;
    jobShortDescription:string;
    organisation: string;
    numberOfOpenings: number;
}