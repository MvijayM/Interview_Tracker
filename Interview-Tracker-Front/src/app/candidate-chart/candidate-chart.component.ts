import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../core/app-config';
import { LocalStorageUtil } from '../core/auth/local-storage-util';

declare var google: any;
@Component({
  selector: 'candidate-chart',
  templateUrl: './candidate-chart.component.html',
  styles: ['./candidate-chart.component.css'],

})


export class CandidateChartComponent implements AfterViewInit {

  selected: number;
  inProgress: number;
  rejected: number;
  applied: number;
  constructor(private _http: HttpClient) { }

  baseUrl: string = AppConfig.API_BASE_PATH;
  ngOnInit(): void {
    this._http.get<any>(this.baseUrl + "/api/job/getJobsByCandidateName/" + LocalStorageUtil.getItem('userName')).subscribe(result => {
      this.selected = result['selected'];
      this.rejected = result['rejected'];
      this.inProgress = result['inProgress'];
      this.applied = result['applied'];
      google.charts.load('current', { 'packages': ['corechart'] });
      google.charts.setOnLoadCallback(this.drawChart);
    })
  }

  @ViewChild('pieChart') pieChart: ElementRef

  drawChart = () => {

    const data = google.visualization.arrayToDataTable([
      ['Status', 'Number of Jobs'],
      ['Selected', this.selected],
      ['Rejected', this.rejected],
      ['InProgress', this.inProgress],
      ['Applied', this.applied],

    ]);

    const options: google.visualization.PieChartOptions = {
      title: 'Status',
      backgroundColor: { fill: 'transparent' },
    };

    const chart = new google.visualization.PieChart(this.pieChart.nativeElement);
    if (this.applied != 0 || this.selected != 0 || this.rejected != 0 || this.inProgress != 0) {
      chart.draw(data, options);
    }
  }

  ngAfterViewInit() {
  }
}