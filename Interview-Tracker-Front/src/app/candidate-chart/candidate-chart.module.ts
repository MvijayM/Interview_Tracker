import { AppConfig } from './../core/app-config';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GoogleChartsModule } from 'angular-google-charts';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { CandidateChartComponent } from './candidate-chart.component';

// const routes: Routes = [{path:'',canActivate: [RolesAuthGuard],component:HrDashboardComponent, data:{accessLevel:AppConfig.ACCESS_LEVEL.HR}}]
@NgModule({
  declarations: [CandidateChartComponent],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    GoogleChartsModule
  ],
  exports: [GoogleChartsModule,CandidateChartComponent]
})
export class CandidateChartModule { }
