import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatLabel } from '@angular/material/form-field';
import { MatSort } from '@angular/material/sort';
import { MatAccordion } from '@angular/material/expansion';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../core/app-config';
import { LocalStorageUtil } from '../core/auth/local-storage-util';
import { ToasterService } from '../toaster.service';
import { AddJobService } from './add-job.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { minExperinceVaidator } from './validators/experience.validator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-job',
  templateUrl: './add-job.component.html',
  styleUrls: ['./add-job.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AddJobComponent implements OnInit, AfterViewInit {
  panelOpenState = false;
  JobName: any;
  NoOfOpenings: any;
  JobShortDescription: any;
  minSalary:number;
  maxSalary:number;
  roundNameCtrl: FormControl = new FormControl('');
  addJobForm: FormGroup;
  JobDescription: any;
  ResponsibilityList: any = [];
  skillsList: string[] = [];
  roundName: string = "";
  personInCharge: string = "";
  
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  Responsibilities: any = [];
  rounds: any = [];
  maxExperience: number;
  minExperience: number;
  submitClicked: boolean = false;
  ngAfterViewInit() {
    this.roundNameCtrl.valueChanges.subscribe(value =>{
      this.roundName = value;
    })
  }
  constructor(private _http: HttpClient,private router:Router, private _service: AddJobService, private toaster: ToasterService, private formBuilder: FormBuilder) { }
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatAccordion, { static: true }) accordion: MatAccordion;
  @ViewChild(MatLabel, { static: true }) label: MatLabel;

  ngOnInit() {
    this.addJobForm = this.formBuilder.group({
      JobName: ['', Validators.required],
      NoOfOpenings: ['', Validators.required],
      minExperience: [0, [Validators.required, Validators.min(0)]],
      maxExperience: [0, [Validators.required, Validators.min(0)]],
      JobShortDescription: ['', Validators.required],
      organisation: ['', Validators.required],
      minSalary:[0,[Validators.required,Validators.min(0)]],
      maxSalary:[0,[Validators.required,Validators.min(0)]]
    }, { validator: minExperinceVaidator });
  }

  private baseUrl = AppConfig.API_BASE_PATH + "/api/job/addNewJob";
  skillChange(skills: string[]) {
    this.skillsList = skills;
  }
  addInterview(event) {
    console.log("submit", event);
    this.submitClicked = true;
    let requestObject = {
      "jobName": this.addJobForm.value.JobName,
      "numberOfOpenings": this.addJobForm.value.NoOfOpenings,
      "jobShortDescription": this.addJobForm.value.JobShortDescription,
      "minExperience": this.addJobForm.value.minExperience,
      "maxExperience": this.addJobForm.value.maxExperience,
      "personInCharge": LocalStorageUtil.getItem("userName"),
      "organisation": this.addJobForm.value.organisation,
      // "jobDescription": this.JobDescription,
      "responsibilities": this.Responsibilities,
      "skillsAndQualification": this.skillsList,
      "rounds": this.rounds,
      "minSalary":this.addJobForm.value.minSalary,
      "maxSalary":this.addJobForm.value.maxSalary
    }
    console.log("request", requestObject);
    if (this.rounds.length > 0) {
      this._service.addJob(requestObject).subscribe(response => {
        if (response.result) {
          this.toaster.success("Job Added Successfully!");
          setTimeout(() => {
            window.location.reload();
          }, 1000);
        } else {
          this.toaster.error("Error in Adding Job");
        }
      })
    }
  }


  addResp(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.Responsibilities.push(value.trim());
    }
    console.log("resp", this.Responsibilities)
    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeResp(resp): void {
    const index = this.Responsibilities.indexOf(resp);

    if (index >= 0) {
      this.Responsibilities.splice(index, 1);
    }
  }


  addRound(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    console.log("roundName", this.roundName, event)
    // Add our fruit
    if ((value || '').trim()) {
      this.rounds.push({
        "roundName": this.roundName,
        "roundDescription": value.trim()
      });
      this.roundName = "";
      this.roundNameCtrl.setValue('');
    }
    console.log("resp", this.rounds)
    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeRound(resp): void {
    const index = this.rounds.indexOf(resp);

    if (index >= 0) {
      this.rounds.splice(index, 1);
    }
  }

}

