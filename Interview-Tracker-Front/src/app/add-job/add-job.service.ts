import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppConfig } from '../core/app-config';

@Injectable({
  providedIn: 'root'
})
export class AddJobService {

  baseUrl: string = AppConfig.API_BASE_PATH + '/api/job/addNewJob'
  constructor(private _http: HttpClient) { }

  addJob(requestObject: Object): Observable<any> {
    return this._http.post(this.baseUrl, requestObject);
  }

}