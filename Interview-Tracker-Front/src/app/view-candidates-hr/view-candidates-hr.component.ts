import { Component, OnInit } from '@angular/core';
import { SearchColumnDef, GridActionDef } from './../common-component/search-page/models/search-column-def';
import { SearchApi } from '../common-component/search-page/api/search-api';
import { CandidateResponseModel } from './candidatesResponseModel';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { AppConfig } from '../core/app-config';
import { MatDialog } from '@angular/material/dialog';
import { InterviewShedulerDialogComponent } from '../interview-sheduler-dialog/interview-sheduler-dialog.component';
import { FeedbackDialogComponent } from '../feedback-dialog/feedback-dialog.component';
import { ToasterService } from '../toaster.service';
import { SharedService } from '../common-component/shared.service';


@Component({
  selector: 'app-view-candidates-hr',
  templateUrl: './view-candidates-hr.component.html',
  styleUrls: ['./view-candidates-hr.component.css']
})

export class ViewCandidatesHrComponent implements OnInit {

  displayColumnDefenition: SearchColumnDef[] = [
    { columnId: 'candidateName', columnName: 'Candidate Name' },
    { columnId: 'emailId', columnName: 'Candidate EmailId' },
    { columnId: 'roundNumber', columnName: 'Round Number' },
    { columnId: 'status', columnName: 'Status' },
    { columnId: 'matchingPercentage', columnName: 'Match Percentage' }

  ];
  rowActions: GridActionDef[] = [
    {
      actionId: 'Select',
      actionName: 'Select'
    },
    {
      actionId: 'Reject',
      actionName: 'Reject'
    }
  ]
  jobIds: string
  api: SearchApi<CandidateResponseModel>
  placeholderText: string = "Search for Candidate Name"
  cand: CandidateResponseModel[]
  ready: boolean = false;
  feedback: string;
  date: Date
  time: string
  refreshGrid: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private baseUrl = AppConfig.API_BASE_PATH + "/api/candidate/getCandidatesByJobId?jobid="
  constructor(private _http: HttpClient, private router: Router, private route: ActivatedRoute,
    private dialog: MatDialog, private toaster: ToasterService, private sharedService: SharedService) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(queryParams => {

      this.jobIds = queryParams['jobid'];
      console.log(this.sharedService.noOfOpenings)
      this.api = new viewCandidateHrApi(this._http, this.jobIds);
      this.ready = true;
    })


    // 
  }
  params: any[];

  selectOrRejectMultipleCandidates(event) {
    if (event.action === 'Select') {
      if (event.data.length > event.data[0].numberOfOpenings) {
        this.toaster.error('Selected Candidate count is greater than Number of Openings.');
      } else {
        this.selectCandidates(event, true);
      }
    } else if (event.action === 'Reject') {
      this.rejectCandidates(event);
    }
  }

  onAction(event) {
    console.log(event);
    if (event.action == 'Select') {
      this.selectCandidates(event, event.data['totalRounds'] > event.data['roundNumber']);
    };
    if (event.action == 'Reject') {
      this.rejectCandidates(event);
    };
  }

  private rejectCandidates(event) {
    const dialogRef = this.dialog.open(FeedbackDialogComponent, {
      width: '400px',
      data: this.feedback
    });
    dialogRef.afterClosed().subscribe(response => {
      if (response.result) {
        this.feedback = response.data
        if (event.data instanceof Array) {
          event.data[0]['feedBack'] = this.feedback;
        } else {
          event.data['feedBack'] = this.feedback;
        }
        const reqModel = (event.data instanceof Array) ? event.data : [event.data];
        this._http.post<any>(AppConfig.API_BASE_PATH + "/api/candidate/rejectCandidate", reqModel).subscribe(result => {
          console.log(result)
          if (result.result) {
            this.refreshGrid.next(true);
          }
        });
      }
    });
  }

  private selectCandidates(event, openDialog: boolean) {

    if (openDialog) {
      const dialogRef = this.dialog.open(InterviewShedulerDialogComponent, {
        width: '400px',
        data: { date: this.date, time: this.time }
      });

      dialogRef.afterClosed().subscribe(response => {
        if (response && response.result) {
          this.date = response.data['date'];
          this.time = response.data['time'];
          if (event.data instanceof Array) {
            event.data[0]['sheduledDate'] = this.date;
            event.data[0]['time'] = this.time;
          }
          else {
            event.data['sheduledDate'] = this.date;
            event.data['time'] = this.time;
          }
          this.makeSelectCall(event);
        }


      });
    } else {
      this.makeSelectCall(event);
    }
  }

  private makeSelectCall(event) {
    const reqModel = (event.data instanceof Array) ? event.data : [event.data];
    this._http.post<any>(AppConfig.API_BASE_PATH + "/api/candidate/selectCandidate", reqModel).subscribe(result => {
      if (result.result) {
        this.toaster.success('Candidate status Updated');
        this.refreshGrid.next(true);
      } else {
        this.toaster.error('Candidate status Not Updated');
      }
    });
  }




  navigateToCandidateProfile(candidate: CandidateResponseModel) {
    this.router.navigate(['../../profile'], {
      queryParams: {
        userName: candidate.candidateName
      },
      relativeTo: this.route
    })
  }
}


export class viewCandidateHrApi implements SearchApi<CandidateResponseModel>{

  constructor(private _http: HttpClient, private jobIds: string) { }

  private baseUrl = AppConfig.API_BASE_PATH + "/api/candidate/getCandidatesByJobId?jobid=" + this.jobIds
  search(keyword: string, offSet: number): Observable<CandidateResponseModel[]> {

    return this._http.get<CandidateResponseModel[]>(AppConfig.API_BASE_PATH + "/api/candidate/getAllCandidatesLikeName/?jobId=" + this.jobIds + "&candidateName=" + keyword + "&offset=" + offSet);


  }
}
