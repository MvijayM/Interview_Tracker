import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewCandidatesHrComponent } from './view-candidates-hr.component';

describe('ViewCandidatesHrComponent', () => {
  let component: ViewCandidatesHrComponent;
  let fixture: ComponentFixture<ViewCandidatesHrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewCandidatesHrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewCandidatesHrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
