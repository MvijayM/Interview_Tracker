import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppConfig } from '../core/app-config';
import { LocalStorageUtil } from '../core/auth/local-storage-util';
import { profileDetails } from './model/profile.models';

@Injectable({
  providedIn: 'root'
})

export class ProfileService {
  baseUrl: string = AppConfig.API_BASE_PATH + '/api/user'
  constructor(private _http: HttpClient) { }

  saveProfile(requestObject: Object): Observable<any> {
    return this._http.post<profileDetails>(this.baseUrl + "/saveProfile", requestObject);
  }

  viewProfile(userName: string): Observable<profileDetails> {
    return this._http.get<profileDetails>(this.baseUrl + "/getUserProfile/" + userName);
  }
}
