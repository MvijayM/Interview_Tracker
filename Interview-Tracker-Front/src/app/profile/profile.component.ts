import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LocalStorageUtil } from '../core/auth/local-storage-util';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { IndividualExperience } from './model/profile.models';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { faEnvelope, faUser, faUserTag, faTransgenderAlt, faLayerGroup, faGraduationCap, faCalendarAlt, faUserCircle, faEdit } from '@fortawesome/free-solid-svg-icons';
import { ProfileService } from './profile.service';
import { ToasterService } from '../toaster.service';
import { CanDeactivateApi } from '../core/guards/confirmation-guard.api';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../common-component/confirmation-dialog/confirmation-dialog.component';
import { mapTo } from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, AfterViewInit, CanDeactivateApi {
  companyExpList: IndividualExperience[] = [];
  companyName: string = "";
  removable: boolean = true;
  isEditAllowed: boolean = false;
  ready: boolean = false;
  fontwesomeIcon: any = {
    faEnvelope: faEnvelope,
    faUser: faUser,
    faUserTag: faUserTag,
    faTransgenderAlt: faTransgenderAlt,
    faGraduationCap: faGraduationCap,
    faLayerGroup: faLayerGroup,
    faCalendarAlt: faCalendarAlt,
    faUserCircle: faUserCircle,
    faEdit: faEdit
  }
  companyNameFormControl: FormControl = new FormControl('', Validators.required);
  selectable: boolean = true;
  formDisabled: boolean = false;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  expValidation: boolean = false;
  constructor(private route: ActivatedRoute, private formBuilder: FormBuilder, private _service: ProfileService,
    private toaster: ToasterService, private dialog: MatDialog) { }
  isSaved(): Promise<boolean> {
    if (this.profileForm.disabled)
      return new Promise((resolve, reject) => resolve(true));
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '300px',
      data: {
        content: "Changes won't be saved. Do you want to continue?",
        type: 'warn',
        header: 'Unsaved Changes!'
      }
    });
    dialogRef.disableClose = true;
    return dialogRef.afterClosed().toPromise();
  }
  userName: string;
  roles: string[] = [];
  profileForm: FormGroup;
  gender: FormControl = new FormControl();
  skillsList: string[] = ['Spring Boot', 'Angular'];
  ngAfterViewInit() {
  }
  ngOnInit(): void {
    this.route.queryParams.subscribe(param => {
      this.userName = param['userName'];
      this.isEditAllowed = LocalStorageUtil.getItem('userName') === this.userName;
    });
    this.viewJobProfile();
  }

  viewJobProfile() {
    this._service.viewProfile(this.userName).subscribe(result => {
      this.profileForm = this.formBuilder.group({
        userName: [{ value: this.userName, disabled: true }],
        roles: [{ value: result.roles, disabled: true }],
        gender: [result.gender?result.gender:'NA'],
        email: [{ value: result.email, disabled: true }],
        highestQualification: [result.highestQualification],
        totalExp: [result.totalExp],
        individualExperience: [0, Validators.min(0)]
      });
      this.profileForm.disable();
      this.formDisabled = true;
      this.ready = true;
      this.skillsList = result.skills;
      this.companyExpList = result.companyExpList;
      this.companyNameFormControl.valueChanges.subscribe(companyName => {
        this.companyName = companyName;
      });
      this.profileForm.get('totalExp').valueChanges.subscribe(value => {
        if (!value || value === 0) {
          this.profileForm.get('individualExperience').setValue(0);
          this.companyNameFormControl.setValue('');
          this.companyExpList = [];
        }
      })
    });
  }


  enableForm() {
    if (this.profileForm.disabled) {
      this.profileForm.enable();
      this.profileForm.get('userName').disable();
      this.profileForm.get('roles').disable();
      this.profileForm.get('email').disable();
      this.formDisabled = false;
    }
  }
  addCompanyExp(event: MatChipInputEvent,experience: number): void {
    const input = event?event.input:null;
    const value  = event? event.value: experience +'';
    console.log("companyName", this.companyName, event)
    // Add our fruit
    const nameList = this.companyExpList.map(companyExp => companyExp.companyName);
    if ((value || '').trim() && this.profileForm.get('individualExperience').valid && this.companyNameFormControl.valid && !nameList.includes(this.companyName)) {
      this.companyExpList.push({
        "companyName": this.companyName,
        "companyExp": Number(value.trim())
      });
      this.companyNameFormControl.setValue("");
    }
    console.log("resp", this.companyExpList)
    // Reset the input value
    if (input) {
      input.value = '';
    }
    let totalExpMonths = 0;
    this.companyExpList.forEach(companyExp => {
      totalExpMonths += companyExp.companyExp;
    });
    this.expValidation = totalExpMonths > this.profileForm.get('totalExp').value;
  }

  removeCompany(company: IndividualExperience): void {
    const index = this.companyExpList.indexOf(company);

    if (index >= 0) {
      this.companyExpList.splice(index, 1);
    }
  }

  skillChange(skills: string[]) {
    this.skillsList = skills;
  }

  saveProfile() {
    let requestObject = {};
    requestObject = {
      "userName": this.profileForm.controls["userName"].value,
      "roles": this.profileForm.controls["roles"].value,
      "gender": this.profileForm.value.gender,
      "email": this.profileForm.controls["email"].value,
      "highestQualification": this.profileForm.value.highestQualification,
      "totalExp": this.profileForm.value.totalExp,
      "skills": this.skillsList,
      "companyExpList": this.companyExpList
    };
    console.log("requestObject", requestObject)
    this._service.saveProfile(requestObject).subscribe(response => {
      if (response.result) {
        this.toaster.success("Profile Saved Successfully!");
        this.disableForm();
      } else {
        this.toaster.error("Error in Saving Profile");
      }
    })

  }

  cancelProfileEdit() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '300px',
      data: {
        content: "Changes won't be saved. Do you want to continue?",
        type: 'warn',
        header: 'Unsaved Changes!'
      }
    });
    dialogRef.disableClose = true;
    dialogRef.afterClosed().subscribe(response => {
      if (response) {
        this.disableForm();
        console.log(this.profileForm.disabled);
      }
    });
  }
  disableForm() {
    this.formDisabled = !this.formDisabled;
        this.profileForm.disable();
        this.profileForm.get('userName').disable();
        this.profileForm.get('roles').disable();
        this.profileForm.get('email').disable();
  }
}
