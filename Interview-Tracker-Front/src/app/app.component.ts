import { LoadingService } from './common-component/loading-service';
import { LocalStorageUtil } from './core/auth/local-storage-util';
import { Component, AfterViewInit, ChangeDetectorRef, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit, OnDestroy, OnInit {
  isLoading: boolean = false;
  $subscription: Subscription;
  constructor(private loadingService: LoadingService, private changeDetector: ChangeDetectorRef) { }

  ngAfterViewInit() {
    this.$subscription = this.loadingService.loadingSubject.subscribe(isLoading => {
      this.isLoading = isLoading;
      this.changeDetector.detectChanges();
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.$subscription) this.$subscription.unsubscribe();
  }
}
