import { ToasterService } from './../toaster.service';
import { LocalStorageUtil } from './../core/auth/local-storage-util';
import { ConfirmationDialogComponent } from './../common-component/confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnDestroy, ChangeDetectorRef, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd, ResolveStart } from '@angular/router';
import { MediaMatcher } from '@angular/cdk/layout';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnDestroy, OnInit {

  mobileQuery: MediaQueryList;
  shouldRun = true;
  showDashBoard: boolean = false;
  isHrDashBoard: boolean = LocalStorageUtil.getItem('roles') === 'HR';
  isCandidateDashBoard: boolean = LocalStorageUtil.getItem('roles') === 'CANDIDATE';
  isAdmin: boolean = LocalStorageUtil.getItem('roles') === 'ADMIN';
  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
    private toaster: ToasterService, private router: Router, public dialog: MatDialog, private route: ActivatedRoute) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.router.events.subscribe((routeData: any) => {
      if (routeData instanceof NavigationEnd) {
        this.showDashBoard = routeData.urlAfterRedirects === '/home';
        if (routeData.urlAfterRedirects === '/login' && LocalStorageUtil.getItem('userName')) {
          LocalStorageUtil.clearAll();
          this.toaster.success('Logged out successfully');
        }
      }
    });
  }

  ngOnInit() {
    this.showDashBoard = this.route.snapshot['_routerState'].url === '/home';
  }
  homeRoute() {
    this.showDashBoard = this.route.snapshot['_routerState'].url === '/home';
  }
  profileRouting() {
    this.showDashBoard = false;
    this.router.navigate(['/home/profile'], {
      queryParams: {
        userName: LocalStorageUtil.getItem('userName')
      }
    })
  }

  navigateToPage(linkToPage) {
    if (linkToPage.currentTarget.pathname === '/logout') {
      LocalStorageUtil.clearAll();
      this.router.navigateByUrl('/login');
    }
  }

  logout() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '250px',
      data: {
        content: 'Confirm Logout?'
      }
    });
    dialogRef.afterClosed().subscribe(confirmation => {
      if (confirmation) {
        this.router.navigateByUrl('/login');
      }
    })
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
}
