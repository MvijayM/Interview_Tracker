import { JobStatusModel } from './models/hr-dashboard';
import { LocalStorageUtil } from './../core/auth/local-storage-util';
import { HrDashboardService } from './hr-dashboard.service';
import { of } from 'rxjs';
import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
@Component({
  selector: 'hr-dashboard',
  templateUrl: './hr-dashboard.component.html',
  styleUrls: ['./hr-dashboard.component.css']
})
export class HrDashboardComponent implements OnInit {
  @ViewChild('jobStatusChart') jobStatusChart: ElementRef
  @ViewChild('individualChart') individualChart: ElementRef;
  @ViewChild('numberOfPositionsChart') numberOfPositionsChart: ElementRef;
  mainPieChart: JobStatusModel;
  statusEnabled: boolean = false;
  individualChartEnabled: boolean = false;
  constructor(private hrDashBoardService: HrDashboardService) { }
  ngOnInit() {
  }

  ngAfterViewInit() {
    this.hrDashBoardService.getJobsGroupedByStatus(LocalStorageUtil.getItem('userName')).subscribe(response => {
      this.mainPieChart = response;
      google.charts.load('current', { 'packages': ['corechart'] });
      google.charts.setOnLoadCallback(this.drawStatusChart);
    });
  }

  drawStatusChart = () => {
    const data = google.visualization.arrayToDataTable([
      ['Jobs', 'Status'],
      ['Closed', this.mainPieChart['closed']],
      ['Open', this.mainPieChart['open']]
    ]);

    const options: google.visualization.PieChartOptions = {
      title: 'Responsible Jobs',
      is3D: true,      
      backgroundColor: { fill: 'transparent' }
    };

    const chart = new google.visualization.PieChart(this.jobStatusChart.nativeElement);
    google.visualization.events.addListener(chart, 'select', () => {
      this.statusEnabled = false;
      this.individualChartEnabled = false;
      this.statusHandler(chart, data);
    });
    if (this.mainPieChart['open'] != 0 || this.mainPieChart['closed'] != 0) {
      chart.draw(data, options);
    }
  };

  statusHandler(chart: google.visualization.PieChart, data: any) {
    let selectedItem = chart.getSelection()[0];
    if (selectedItem) {
      let status = data.getValue(selectedItem.row, 0);
      this.showAllJobsPerStatusChart(status);
    }
  }

  showAllJobsPerStatusChart(status: string) {
    if (status !== 'Closed') {

      this.hrDashBoardService.getAllCandidatesPerJobStatus(status.toUpperCase(), LocalStorageUtil.getItem('userName')).subscribe(response => {
        this.individualChartEnabled = true;
        const data = google.visualization.arrayToDataTable([
          ['Job Name', 'Number of Candidates', 'Job ID'],
          ['Sample', 0, 1]
        ]);
        response.forEach(job => {
          data.addRow([job['jobName'], job['numberOfCandidates'], job['jobId']]);
        });

        const options: google.visualization.PieChartOptions = {
          title: 'Candidates Per Job',
          is3D: true,
          backgroundColor: { fill: 'transparent' }
        };

        const chart = new google.visualization.PieChart(this.individualChart.nativeElement);
        google.visualization.events.addListener(chart, 'select', () => {
          this.individualJobHandler(chart, data);
        });
        chart.draw(data, options);
      });
    }
  };

  individualJobHandler(chart: google.visualization.PieChart, data) {
    var selectedPie = chart.getSelection()[0];
    if (selectedPie) {
      var jobId = data.getValue(selectedPie.row, 2);
      this.statusEnabled = false;
      this.showPositionInformationOfJob(jobId);
    }
  };

  showPositionInformationOfJob(jobId: number) {
    this.hrDashBoardService.getJobOpeningStatus(jobId).subscribe(response => {
      this.statusEnabled = true;
      const data = google.visualization.arrayToDataTable([
        ['Status', 'Number of positions filled'],
        ['Filled', response['filled']],
        ['Yet to be filled', response['remaining']]
      ]);

      const options: google.visualization.PieChartOptions = {
        title: 'Individual Job Opening Status',
        backgroundColor: { fill: 'transparent' },
        pieHole: 0.4
      };
      const chart = new google.visualization.PieChart(this.numberOfPositionsChart.nativeElement);
      chart.draw(data, options);
    });
  }
}
