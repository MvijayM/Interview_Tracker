import { Observable } from 'rxjs';
import { AppConfig } from './../app-config';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as socketIo from 'socket.io-client';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
export interface MessageModel {
    from:string;
    to:string;
    message:string;
}
@Injectable({
    providedIn:'root'
})
export class SocketService {
    socket;
    constructor(private _http: HttpClient){}
    establistConnection() {
        if(!this.socket) {
            this.socket = socketIo(AppConfig.SOCKET_ENDPOINT);
        }
    }

    sendMessage(data: MessageModel) {
        this.socket.emit('send',data);
    }

    receiveMessage(userName:string):Observable<MessageModel> {
        const observer = new Observable<MessageModel>(observer =>{
            this.socket.on('recieve/'+ userName,(message:MessageModel) =>{
                observer.next(message);
            });
            return () => {
                this.socket.disconnect();
            }
        });
        return observer;
    }

    publishToEndPoint(endPoint:string,data:any) {
        this.socket.emit(endPoint,data);
    }

    consumeOnEndPoint(endPoint) {
        this.socket.on(endPoint,() =>{
            
        })
    }
}