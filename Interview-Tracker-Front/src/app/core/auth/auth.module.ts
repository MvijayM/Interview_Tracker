import { RolesAuthGuard } from './guard/roles-auth-guard';
import { AuthGuard } from './guard/auth-guard';
import { AuthService } from './service/auth-service';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [],
  providers: [AuthService,AuthGuard,RolesAuthGuard]
})
export class AuthModule {
  static forRoot(): ModuleWithProviders<AuthModule> {
  return {
    ngModule: AuthModule,
    providers: [
      {provide: AuthService, useClass: AuthService },
      {provide: AuthGuard, useClass: AuthGuard },
      {provide: RolesAuthGuard, useClass: RolesAuthGuard}
    ]
  };
 }
}
