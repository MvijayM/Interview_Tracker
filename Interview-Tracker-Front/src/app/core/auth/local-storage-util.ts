export class LocalStorageUtil {

    public static setItem(key: string, value: any) {
        sessionStorage.setItem(key,value);
    }

    public static getItem(key: string): any {
        return sessionStorage.getItem(key);
    }

    public static removeItem(key: string) {
        sessionStorage.removeItem(key);
    }

    public static clearAll(): void {
        LocalStorageUtil.removeItem("jwtToken");
        LocalStorageUtil.removeItem("userName");
        LocalStorageUtil.removeItem("roles");
    }
}
