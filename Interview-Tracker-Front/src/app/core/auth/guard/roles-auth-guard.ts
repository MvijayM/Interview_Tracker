import { LocalStorageUtil } from './../local-storage-util';
import { ToasterService } from './../../../toaster.service';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { InformationDisplayDialogComponent } from 'src/app/common-component/information-display-dialog/information-display-dialog.component';

@Injectable()
export class RolesAuthGuard implements CanActivate{
    constructor(private toaster: ToasterService,private dialog: MatDialog) {}
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | import("@angular/router").UrlTree | import("rxjs").Observable<boolean | import("@angular/router").UrlTree> | Promise<boolean | import("@angular/router").UrlTree> {
        if(!route.data || !route.data.accessLevel) {
            return true;
        }
        const userRoles: Array<string> = LocalStorageUtil.getItem("roles");
        if(userRoles.includes(route.data.accessLevel)) {
            return true;
        } else {
            const dialogRef = this.dialog.open(InformationDisplayDialogComponent,{
                width:'300px',
                data:{
                  content: "Sorry you don't have authority to view this page.",
                  type: 'warn',
                  header:'Authority Issue!'
                }
              });
              dialogRef.afterClosed().subscribe(response =>{
                return false;
              });
        }
    }
}
