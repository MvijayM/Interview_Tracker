import { Injectable } from '@angular/core';
import { AuthService } from './../service/auth-service';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
@Injectable()
export class AuthGuard implements  CanActivate {
    constructor(private authService: AuthService, private router: Router) {}
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | import("@angular/router").UrlTree | import("rxjs").Observable<boolean | import("@angular/router").UrlTree> | Promise<boolean | import("@angular/router").UrlTree> {
        if(this.authService.isLoggedIn()) return true;
        this.router.navigate(['/login'],{
            queryParams :{
                return: state.url
            }
        });
        return false;
    }
}
