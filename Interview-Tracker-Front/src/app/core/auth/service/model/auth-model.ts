import { AuthUserResponse } from './auth-user-response.';
export interface AuthModel {
    authResult: boolean;
    verificationResult:boolean;
    jwt: string;
    userResponse:AuthUserResponse;
}