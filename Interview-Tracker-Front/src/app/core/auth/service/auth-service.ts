import { LocalStorageUtil } from './../local-storage-util';
import { map, catchError } from 'rxjs/operators';
import { AuthModel } from './model/auth-model';
import { AppConfig } from './../../app-config';
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthUserResponse } from './model/auth-user-response.';
import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private baseUrl: string = AppConfig.API_BASE_PATH+'/api/user';
  return:string='/';
  constructor(private _http: HttpClient,private route: ActivatedRoute) { 
    this.route.queryParams.subscribe(params =>{
      this.return = params['return'] || '/home';
    });
  }
  public isLoggedIn(): boolean {
    return !!LocalStorageUtil.getItem('jwtToken');
  }
  public perfomLogin(userDetails: {[key:string]: string}): Observable<{[key:string]:boolean}> {
    return this._http.post<AuthModel>(this.baseUrl+'/noAuth/login',userDetails,{headers: {'loginRequest':'login'}}).pipe(map(authResponse =>{
      if(authResponse['authResult'] && authResponse['verificationResult'])
        this.setLocalStorage(authResponse);
      else 
        LocalStorageUtil.clearAll();
      return {'authResult': authResponse['authResult'], 'verificationResult': authResponse['verificationResult']};
    }));
  }

  public setLocalStorage(authReponse: AuthModel): void {
    LocalStorageUtil.setItem("jwtToken",authReponse['jwt']);
    LocalStorageUtil.setItem("userName",authReponse['userResponse']['userName']);
    LocalStorageUtil.setItem("roles",authReponse['userResponse']['roles']);
  }

  public checkValidity(userName: string): Observable<AuthUserResponse> {
    return this._http.get<AuthUserResponse>(this.baseUrl+'/getUserByName/'+ userName).pipe(map(authResponse => authResponse),catchError( error =>{
      if(error.status === 401) {
        LocalStorageUtil.clearAll();
      }
      return throwError(error);
    }));
  }

}
