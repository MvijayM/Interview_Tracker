export interface ApiResult {
    result: boolean;
    message: string;
}